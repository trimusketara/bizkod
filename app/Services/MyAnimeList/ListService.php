<?php

namespace App\Services\MyAnimeList;


use App\ListEntry;

class ListService
{
    public static function getUserList($username)
    {
        $results = [];

        $client = new \GuzzleHttp\Client(['proxy' => 'http://proxy.vts.su.ac.rs:8080']);
        $resp = $client->request('GET', "http://myanimelist.net/malappinfo.php?u=$username&status=all&type=anime", ['verify' => false]);
        $data = new \SimpleXMLElement($resp->getBody());

        foreach ($data->anime as $anime)
        {
            $entry = new ListEntry();
            $entry->id =     intval((string) $anime->series_animedb_id);
            $entry->title =  (string) $anime->series_title;
            $entry->score =  intval((string) $anime->my_score);
            $entry->status = (string) $anime->my_status;

            $results[] = $entry;
        }

        return $results;
    }
}