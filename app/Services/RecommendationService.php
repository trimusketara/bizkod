<?php

namespace App\Services;


use App\Anime;
use App\Genre;
use App\GenreInfo;
use App\Services\MyAnimeList\ListService;

class RecommendationService
{
    public static function sortGenresByWeightedScore($a, $b)
    {
        return ($a->weightedScore > $b->weightedScore) ? -1 : 1;
    }

    public static function normalizeGenres($genres)
    {
        foreach($genres as $genre)
        {
            $genre->averageScore = $genre->scoreSum / $genre->titleCount;
        }

        $titleCountMax = 0;
        foreach($genres as $genre) if ($titleCountMax < $genre->titleCount) $titleCountMax = $genre->titleCount;

        $titleCountMin = $titleCountMax;
        foreach($genres as $genre) if ($titleCountMin > $genre->titleCount) $titleCountMin = $genre->titleCount;

        foreach($genres as $genre)
        {
            $genre->weight = ($genre->titleCount - $titleCountMin) / ($titleCountMax - $titleCountMin) * 10;
            $genre->weightedScore = (3 * $genre->averageScore + 2 * $genre->weight) / 5;
        }

        return $genres;
    }

    public static function getGenres($list, $minWeightedScore)
    {
        $genreList = [];
        foreach ($list as $item)
        {
            $anime = Anime::where('mal_id', $item->id)->first();
            if (!is_null($anime) && $item->score != 0)
            {
                foreach ($anime->genres as $genre)
                {
                    if (!array_key_exists($genre->name, $genreList))
                    {
                        $genreInfo = new GenreInfo();
                        $genreInfo->id = $genre->id;
                        $genreInfo->name = $genre->name;

                        $genreList[$genre->name] = $genreInfo;
                    }

                    $genreList[$genre->name]->scoreSum += $item->score;
                    $genreList[$genre->name]->titleCount++;
                }
            }
        }

        $genreList = self::normalizeGenres($genreList);
        usort($genreList, ["App\\Services\\RecommendationService", "sortGenresByWeightedScore"]);

        $finalList = [];
        foreach($genreList as $genre) if ($genre->weightedScore >= $minWeightedScore) $finalList[] = $genre;
        return $finalList;
    }

    public static function forUser($list, $minWeightedScore)
    {
        $totalResults = collect([]);
        $existingIds = [];
        $recommendedIds = [];
        foreach($list as $item) $existingIds[] = $item->id;

        $genres = self::getGenres($list, $minWeightedScore);

        foreach ($genres as $genreInfo)
        {
            $genre = Genre::find($genreInfo->id);
            foreach ($genre->animes->take(100) as $anime)
            {
                if (!in_array($anime->mal_id, $existingIds) and !in_array($anime->mal_id, $recommendedIds))
                {
                    $totalResults[] = $anime;
                    $recommendedIds[] = $anime->mal_id;
                }
            }
        }

        $membersMax = 0;
        foreach($totalResults as $result) if ($result->members > $membersMax) $membersMax = $result->members;

        $membersMin = $membersMax;
        foreach($totalResults as $result) if ($result->members < $membersMin) $membersMin = $result->members;

        foreach($totalResults as $result)
        {
            $result->normalizedPopularity = ($result->members - $membersMin) / ($membersMax - $membersMin) * 10;
            $result->finalWeightedScore = ($result->rating + $result->normalizedPopularity) / 2;
        }

        return $totalResults->sortByDesc('finalWeightedScore')->take(52);
    }
}