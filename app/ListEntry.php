<?php
/**
 * Created by PhpStorm.
 * User: studenti
 * Date: 4/29/2017
 * Time: 1:31 PM
 */

namespace App;


class ListEntry
{
    public $id;
    public $title;
    public $score;
    public $status;
}