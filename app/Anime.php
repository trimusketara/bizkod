<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anime extends Model
{
    protected $table = 'anime';
    public $timestamps = false;

    public $normalizedPopularity;
    public $finalWeightedScore;

    public function genres()
    {
        return $this->belongsToMany('App\Genre');
    }
}
