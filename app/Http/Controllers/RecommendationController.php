<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\MyAnimeList\ListService;
use App\Services\RecommendationService;

class RecommendationController extends BaseController
{
    public function recommend($username)
    {
        $list = ListService::getUserList($username);
        $anime = RecommendationService::forUser($list, 6);
        $genres = RecommendationService::getGenres($list, 6);
        return view('results', ['anime' => $anime, 'username' => $username, 'genres' => $genres]);
    }
}