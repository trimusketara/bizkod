<?php

namespace App;


class GenreInfo
{
    public $id;
    public $name;
    public $scoreSum = 0;
    public $titleCount = 0;

    public $averageScore;
    public $weight;
    public $weightedScore;
}