<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anime', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->enum('type', ['Movie', 'TV', 'OVA', 'Special', 'Music', 'ONA']);
            $table->integer('episodes');
            $table->double('rating');
            $table->integer('members');
        });

        Schema::create('genres', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('anime_genre', function(Blueprint $table) {
            $table->integer('anime_id');
            $table->integer('genre_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
