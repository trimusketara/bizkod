var $ = function(id) {
    return document.getElementById(id);
};
function speechBubbleSequence(){
        setTimeout(function(){
            $('#bubble-load').show();
            $('#bubble-load').html("<i>Hi! My name is Yori, and I'm here to help you find the perfect anime!</i>");
        },2000);
        setTimeout(function(){
            $('#bubble-load').hide();
            $('#bubble-load').html("");
        },7000);
        setTimeout(function(){
            $('#bubble-two').show();
            $('#bubble-two').html("<i>Just type in your MyAnimeList username and I'll hook you up ASAP!</i>");
        },9000);
        setTimeout(function(){
            $('#bubble-two').hide();
            $('#bubble-two').html("");
        },14000);
}

$("#post").focus(function() {
    $(this).data("hasfocus", true);
});

$("#post").blur(function() {
    $(this).data("hasfocus", false);
});

$(document.body).keyup(function(ev) {
    if (ev.which === 13 && $("#post").data("hasfocus")) {
        $('#form').submit();
    }
});