var sliderValue=0;
$(function() {
    $('.sample').slider();
    sliderValue=$('.sample').value();
    return sliderValue;
});


var delay=1000;
var timer;
$('.resultDivFirst').hover(function() {
    timer = setTimeout(function(){
        $('.resultDivFirst').addClass('col-md-6');
        $('.resultDivFirst').removeClass('col-md-3');
        $('.resultDivLast').hide();
    }, delay);
}, function(){
    clearTimeout(timer);
    $('.resultDivFirst').addClass('col-md-3');
    $('.resultDivFirst').removeClass('col-md-6');
    $('.resultDivLast').show();
});
//first result div js "on hover", for anime description

$('.resultDivSecond').hover(function() {
    timer = setTimeout(function(){
        $('.resultDivSecond').addClass('col-md-6');
        $('.resultDivSecond').removeClass('col-md-3');
        $('.resultDivLast').hide();
    }, delay);
}, function(){
    clearTimeout(timer);
    $('.resultDivSecond').addClass('col-md-3');
    $('.resultDivSecond').removeClass('col-md-6');
    $('.resultDivLast').show();
});
//second result div js "on hover", for anime description

$('.resultDivThird').hover(function() {
    timer = setTimeout(function(){
        $('.resultDivThird').addClass('col-md-6');
        $('.resultDivThird').removeClass('col-md-3');
        $('.resultDivLast').hide();
    }, delay);
}, function(){
    clearTimeout(timer);
    $('.resultDivThird').addClass('col-md-3');
    $('.resultDivThird').removeClass('col-md-6');
    $('.resultDivLast').show();
});
//third result div js "on hover", for anime description

$('.resultDivLast').hover(function() {
    timer = setTimeout(function(){
        $('.resultDivLast').addClass('col-md-6');
        $('.resultDivLast').removeClass('col-md-3');
        $('.resultDivFirst').hide();
    },  delay);
}, function() {
    $('.resultDivLast').addClass('col-md-3');
    $('.resultDivLast').removeClass('col-md-6');
    $('.resultDivFirst').show();
});
//fourth result div js "on hover", for anime description