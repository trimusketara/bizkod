<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Anime Recommendations</title>
        <link rel="stylesheet" type="text/css" href="/css/homeSearch.css">
    </head>
    <body onload="speechBubbleSequence()">
        <div id="searchBox">
            <form>
                <input id="post" class="center-block" type="text" name="search" placeholder="Your username goes here...">
            </form>
        </div>
    <div id="bubble-load"></div>
        <div id="bubble-two"></div>
    </body>
    <script src="/js/YoriBubbles.js"></script>

    <script src="{{asset('/js/jquery-3.2.1.min.js')}}"></script>
    <script src="/js/welcome.js"></script>
</html>
