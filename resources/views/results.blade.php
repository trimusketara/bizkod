<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Anime Recommendations</title>
    <link rel="stylesheet" type="text/css" href="/css/results.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
    <script src="{{asset('/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('/js/bootstrap-slider.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-slider.css">



</head>
<body>


<div>
    <div class="bubble">This is the best I could do <br> on such short notice! <b>D:</b><br>
    </div>
    <img src="/over9000.png" id="over9000img" >
</div>
<div class="container-fluid">
    <div class="user-info">
        <div class="username">Showing recommendations for: <a href="https://myanimelist.net/profile/{{$username}}/">{{$username}}</a></div>
        <div class="genres">
            Favoring genres:
            @foreach($genres as $genre)
                {{$genre->name}}@if (!$loop->last), @endif
            @endforeach
        </div>
    </div>
    <div class="row">
        @foreach($anime as $entry)
            <div class="col-md-3">
                <a href="https://myanimelist.net/anime/{{$entry->mal_id}}">
                    <div class="result-box">
                        <div class="title">{{$entry->title}}</div>
                        <div class="episodes">{{$entry->episodes}} eps</div>
                        <div class="rating">Rating: {{$entry->rating}}</div>
                        <div class="type">{{$entry->type}}</div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>
<script src="/js/bootstrap.js"></script>
<script src="/js/myresults.js"></script>

</body>

</html>
