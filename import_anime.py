import csv
import pymysql.cursors

data = []

with open('anime.csv', encoding="utf8") as csvfile:
	reader = csv.reader(csvfile, delimiter=',', quotechar='"')
	for row in reader:
		if row[0] != 'anime_id':
			row_object = {
				'anime_id': row[0],
				'title': row[1],
				'genres': row[2].split(', '),
				'type': row[3],
				'episodes': int(row[4]) if row[4] != 'Unknown' else 0,
				'rating': float(row[5]) if row[5].strip() != '' else 0,
				'members': int(row[6])
				}

			if 'Hentai' in row_object['genres']:
				continue

			print(row_object)		
			data.append(row_object)

print("\n==========\nUkupan broj: {}\n==========\n".format(len(data)))

genres = []
for item in data:
	for genre in item['genres']:
		if not genre in genres and genre.strip() != '':
			genres.append(genre)

print("Zanrovi: {} (ukupno {})".format(genres, len(genres)))			

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='',
                             db='bizkod',
                             charset='utf8mb4',
                             # charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)

with connection.cursor() as cursor:
	print("Truncating tables...")
	sql = "truncate table `anime`"
	cursor.execute(sql)
	sql = "truncate table `genres`"
	cursor.execute(sql)
	sql = "truncate table `anime_genre`"
	cursor.execute(sql)
	print("Done.")

	genre_ids = {};
	for genre in genres:
		print("Inserting genre {}.".format(genre))
		sql = "INSERT INTO `genres` (`name`) VALUES (%s)"
		cursor.execute(sql, (genre))
		genre_ids[genre] = cursor.lastrowid

	print("Genre IDs: {}".format(genre_ids))

	for item in data:
		print("Inserting anime with MAL ID {} ('{}')".format(item['anime_id'], item['title']))
		sql = "INSERT INTO `anime` (`mal_id`, `title`, `type`, `episodes`, `rating`, `members`) VALUES (%s, %s, %s, %s, %s, %s)"
		cursor.execute(sql, (item['anime_id'], item['title'], item['type'], item['episodes'], item['rating'], item['members']))
		connection.commit()
		anime_row_id = cursor.lastrowid
		for genre in item['genres']:
			if genre != '':
				genre_id = genre_ids[genre]
				sql = "INSERT INTO `anime_genre` (`anime_id`, `genre_id`) VALUES (%s, %s)"
				cursor.execute(sql, (anime_row_id, genre_id))
				print("> Adding genre reference: {} (ID {})".format(genre, genre_id))
	connection.commit()	    
connection.close()		


